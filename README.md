# ~/ABRÜPT/PIER LAMPÁS/L’ART DES DILUTIONS/*

La [page de ce livre](https://abrupt.ch/pier-lampas/lart-des-dilutions/) sur le réseau.

## Sur le livre

sur la N113

entre deux ronds-points occupés

dans la douceur du printemps occitan

<br>

un cri un chant une prière

contre l'enfer néolibéral patriarcal colonial

## Sur l'auteur

Artisan des entre-chocs et des entre-mondes. Lutte, vagabonde, écrit dans le sud de l'Europe.

[Un site](https://pierlampas.com) pour faire *livre d'image*.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
