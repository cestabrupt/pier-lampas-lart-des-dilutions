const art = [
  [
    "ayons l’art enfin des stratégies éruptives",
  ],
  [
    "mon chant est immanent&nbsp;:",
    "il perce sous l’épiderme",
  ],
  [
    "bruto&nbsp;!",
  ],
  [
    "ignores-tu que le poème a pour vocation de se fondre au cœur des choses afin de les éclairer&nbsp;?",
    "et que l’action révolutionnaire établit les conditions matérielles de cette lumière&nbsp;?",
  ],

  [
    "c’est ici bas que nous nous battrons",
  ],
  [
    "les passions nerveuses ne nous durent pas",
    "aucune étoile au-dessus de nos têtes",
    "mille soleils, et c’est tout",
  ],
  [
    "c’est la caresse enflammée d’un soleil vengeur sur le front de nos lâchetés",
  ],
  [
    "car en l’absence de géniteurs crédibles et de racines, nous sommes libres",
  ],
  [
    "l’alchimie singulière des enfants du printemps ne suppose aucun relâchement",
  ],
  [
    "il ne s’agira pas de grossir les rangs de la pestilente postmodernité&nbsp;:",
    "notre dilution n’implique aucunement l’abolition des hommes et de leur histoire",
    "mais cette histoire sera déterritorialisée, profane, décoloniale",
  ],
  [
    "car nous sommes tous sœurs et frères de floraison&nbsp;!",
    "et à ce titre&nbsp;: entièrement vierge de rancœur",
  ],
  [
    "et nous enfanterons des trésors sans aucune larme amère,",
    "et des champs de violettes et des soleils de dune",
  ],
  [
    "nous nous ferons artisans des entre-chocs et des entre-mondes&nbsp;!",
  ],
  [
    "notre théâtre est celui des passions animales",
    "et c’est le comble du raffinement",
  ],
  [
    "dans chaque détour, je rencontre la nécessité",
    "d’en finir avec cette société de classes",
  ],
  [
    "et si l’âpreté du monde justifie notre méfiance de l’aube,",
    "prenons pour modèle l’éclosion des bourgeons",
  ],
  [
    "car ici, il reste des hommes, il reste des femmes, et des drapeaux troués",
    "et des soleils vengeurs, et des éclats de poudre et des sentiers rageurs",
  ],
  [
    "cette renaissance nous ouvre la voie d’un monde déjà existant,",
    "un monde opprimé et suffocant,",
    "mais dont la lumière plurielle et viscérale ne saurait taire son évidence",
  ],
  [
    "je suis un champ de lavande",
    "qu’irradie le vin des révolutions",
  ],
  [
    "dans la révolte jaune",
    "et les refrains fatigués",
    "des vieux cortèges syndicaux",
  ],
  [
    "dans ces hymnes vieillots",
    "et dans la résurgence",
    "de la conscience de classe",
  ],
  [
    "dans l’exacerbation du mépris d’état",
    "et dans le dernier sursaut",
    "de la dignité éborgnée",
  ],
  [
    "défoncé à grand coup",
    "de chariot élévateur",
  ],
  [
    "c’est sous le patronage de l’errance et de la déshérence",
    "que je compose mon chant,",
    "c’est la chair impersonnelle et irresponsable qui me convoque",
  ],
  [
    "et j’envie l’innommé",
    "autant que l’innommable",
  ],
  [
    "le monde est un ogre affamé,",
    "je veux qu’il m’avale sans ménagement",
  ],
  [
    "je veux que ma dilution soit indigeste,",
    "qu’il me recrache en mille lambeaux",
  ],
  [
    "je ne serai qu’échappées et gueules ouvertes,",
    "et je me diluerai dans les régions de l’infâme",
    "en me moquant de nos chairs suppliciées",
  ],
  [
    "car l’espérance, j’abdique",
    "je n’ai plus le temps",
    "et je préfère le vin",
  ],
  [
    "suis-je trop sévère avec les étoiles&nbsp;?",
    "je ne le crois pas",
    "c’est que l’âme d’un voyou me paraît plus honnête",
  ],
  [
    "ville crasse et théâtre des atrocités ordinaires",
  ],
  [
    "dans l’histoire concrète d’une main qui se déploie",
    "sur un fusil mitrailleur,",
    "sur ses munitions de perles et de jacinthes...",
  ],
  [
    "construisons méthodiquement",
    "les conditions d’une dialectique",
    "de la terre et du ciel",
  ],
  [
    "les étoiles ne pourront plus se moquer",
    "car elles baigneront dans la même fange",
    "et dans la même ivresse que nous",
  ],
  [
    "mais il génère aussi la grâce",
    "n’ayons plus jamais l’art des compromissions&nbsp;!",
  ],
  [
    "et nous combattrons ainsi le discours managérial",
    "appliqué à la floraison des êtres et des choses",
  ],
  [
    "je veux vivre en bonne intelligence",
    "avec le ficus sauvage faisant corps",
    "avec le pavé napolitain",
  ],
  [
    "c’est que ce poème sera radicalement un poème",
    "de douceur napolitaine et de violence nécessaire",
    "d’amour et de douceur révolutionnaires&nbsp;!",
  ],
  [
    "et ma soif aiguise son objet&nbsp;:",
    "nous sommes le monde",
  ],
  [
    "car l’alchimie moléculaire éventre les destins biaisés",
  ],
  [
    "la chair vive et passionnée",
    "d’un amour qui se confond avec la rage",
  ],
  [
    "dans chaque détour, l’histoire&nbsp;!",
  ],
  [
    "car il faut fondre nietzsche dans marx&nbsp;!",
    "et marx dans rimbaud&nbsp;!",
  ],
  [
    "il faut jeter platon dans les orties&nbsp;!",
    "et installer la poésie dans le cœur des choses&nbsp;!",
  ],
  [
    "nous sommes marxistes et amoureux des précipices&nbsp;!",
  ],
  [
    "c’est une histoire d’herbes folles",
    "dans le cimetière profane du testaccio",
  ]
];

/*! dom-to-image 10-06-2017 */
!function(a){"use strict";function b(a,b){function c(a){return b.bgcolor&&(a.style.backgroundColor=b.bgcolor),b.width&&(a.style.width=b.width+"px"),b.height&&(a.style.height=b.height+"px"),b.style&&Object.keys(b.style).forEach(function(c){a.style[c]=b.style[c]}),a}return b=b||{},g(b),Promise.resolve(a).then(function(a){return i(a,b.filter,!0)}).then(j).then(k).then(c).then(function(c){return l(c,b.width||q.width(a),b.height||q.height(a))})}function c(a,b){return h(a,b||{}).then(function(b){return b.getContext("2d").getImageData(0,0,q.width(a),q.height(a)).data})}function d(a,b){return h(a,b||{}).then(function(a){return a.toDataURL()})}function e(a,b){return b=b||{},h(a,b).then(function(a){return a.toDataURL("image/jpeg",b.quality||1)})}function f(a,b){return h(a,b||{}).then(q.canvasToBlob)}function g(a){"undefined"==typeof a.imagePlaceholder?v.impl.options.imagePlaceholder=u.imagePlaceholder:v.impl.options.imagePlaceholder=a.imagePlaceholder,"undefined"==typeof a.cacheBust?v.impl.options.cacheBust=u.cacheBust:v.impl.options.cacheBust=a.cacheBust}function h(a,c){function d(a){var b=document.createElement("canvas");if(b.width=c.width||q.width(a),b.height=c.height||q.height(a),c.bgcolor){var d=b.getContext("2d");d.fillStyle=c.bgcolor,d.fillRect(0,0,b.width,b.height)}return b}return b(a,c).then(q.makeImage).then(q.delay(100)).then(function(b){var c=d(a);return c.getContext("2d").drawImage(b,0,0),c})}function i(a,b,c){function d(a){return a instanceof HTMLCanvasElement?q.makeImage(a.toDataURL()):a.cloneNode(!1)}function e(a,b,c){function d(a,b,c){var d=Promise.resolve();return b.forEach(function(b){d=d.then(function(){return i(b,c)}).then(function(b){b&&a.appendChild(b)})}),d}var e=a.childNodes;return 0===e.length?Promise.resolve(b):d(b,q.asArray(e),c).then(function(){return b})}function f(a,b){function c(){function c(a,b){function c(a,b){q.asArray(a).forEach(function(c){b.setProperty(c,a.getPropertyValue(c),a.getPropertyPriority(c))})}a.cssText?b.cssText=a.cssText:c(a,b)}c(window.getComputedStyle(a),b.style)}function d(){function c(c){function d(a,b,c){function d(a){var b=a.getPropertyValue("content");return a.cssText+" content: "+b+";"}function e(a){function b(b){return b+": "+a.getPropertyValue(b)+(a.getPropertyPriority(b)?" !important":"")}return q.asArray(a).map(b).join("; ")+";"}var f="."+a+":"+b,g=c.cssText?d(c):e(c);return document.createTextNode(f+"{"+g+"}")}var e=window.getComputedStyle(a,c),f=e.getPropertyValue("content");if(""!==f&&"none"!==f){var g=q.uid();b.className=b.className+" "+g;var h=document.createElement("style");h.appendChild(d(g,c,e)),b.appendChild(h)}}[":before",":after"].forEach(function(a){c(a)})}function e(){a instanceof HTMLTextAreaElement&&(b.innerHTML=a.value),a instanceof HTMLInputElement&&b.setAttribute("value",a.value)}function f(){b instanceof SVGElement&&(b.setAttribute("xmlns","http://www.w3.org/2000/svg"),b instanceof SVGRectElement&&["width","height"].forEach(function(a){var c=b.getAttribute(a);c&&b.style.setProperty(a,c)}))}return b instanceof Element?Promise.resolve().then(c).then(d).then(e).then(f).then(function(){return b}):b}return c||!b||b(a)?Promise.resolve(a).then(d).then(function(c){return e(a,c,b)}).then(function(b){return f(a,b)}):Promise.resolve()}function j(a){return s.resolveAll().then(function(b){var c=document.createElement("style");return a.appendChild(c),c.appendChild(document.createTextNode(b)),a})}function k(a){return t.inlineAll(a).then(function(){return a})}function l(a,b,c){return Promise.resolve(a).then(function(a){return a.setAttribute("xmlns","http://www.w3.org/1999/xhtml"),(new XMLSerializer).serializeToString(a)}).then(q.escapeXhtml).then(function(a){return'<foreignObject x="0" y="0" width="100%" height="100%">'+a+"</foreignObject>"}).then(function(a){return'<svg xmlns="http://www.w3.org/2000/svg" width="'+b+'" height="'+c+'">'+a+"</svg>"}).then(function(a){return"data:image/svg+xml;charset=utf-8,"+a})}function m(){function a(){var a="application/font-woff",b="image/jpeg";return{woff:a,woff2:a,ttf:"application/font-truetype",eot:"application/vnd.ms-fontobject",png:"image/png",jpg:b,jpeg:b,gif:"image/gif",tiff:"image/tiff",svg:"image/svg+xml"}}function b(a){var b=/\.([^\.\/]*?)$/g.exec(a);return b?b[1]:""}function c(c){var d=b(c).toLowerCase();return a()[d]||""}function d(a){return a.search(/^(data:)/)!==-1}function e(a){return new Promise(function(b){for(var c=window.atob(a.toDataURL().split(",")[1]),d=c.length,e=new Uint8Array(d),f=0;f<d;f++)e[f]=c.charCodeAt(f);b(new Blob([e],{type:"image/png"}))})}function f(a){return a.toBlob?new Promise(function(b){a.toBlob(b)}):e(a)}function g(a,b){var c=document.implementation.createHTMLDocument(),d=c.createElement("base");c.head.appendChild(d);var e=c.createElement("a");return c.body.appendChild(e),d.href=b,e.href=a,e.href}function h(){var a=0;return function(){function b(){return("0000"+(Math.random()*Math.pow(36,4)<<0).toString(36)).slice(-4)}return"u"+b()+a++}}function i(a){return new Promise(function(b,c){var d=new Image;d.onload=function(){b(d)},d.onerror=c,d.src=a})}function j(a){var b=3e4;return v.impl.options.cacheBust&&(a+=(/\?/.test(a)?"&":"?")+(new Date).getTime()),new Promise(function(c){function d(){if(4===g.readyState){if(200!==g.status)return void(h?c(h):f("cannot fetch resource: "+a+", status: "+g.status));var b=new FileReader;b.onloadend=function(){var a=b.result.split(/,/)[1];c(a)},b.readAsDataURL(g.response)}}function e(){h?c(h):f("timeout of "+b+"ms occured while fetching resource: "+a)}function f(a){console.error(a),c("")}var g=new XMLHttpRequest;g.onreadystatechange=d,g.ontimeout=e,g.responseType="blob",g.timeout=b,g.open("GET",a,!0),g.send();var h;if(v.impl.options.imagePlaceholder){var i=v.impl.options.imagePlaceholder.split(/,/);i&&i[1]&&(h=i[1])}})}function k(a,b){return"data:"+b+";base64,"+a}function l(a){return a.replace(/([.*+?^${}()|\[\]\/\\])/g,"\\$1")}function m(a){return function(b){return new Promise(function(c){setTimeout(function(){c(b)},a)})}}function n(a){for(var b=[],c=a.length,d=0;d<c;d++)b.push(a[d]);return b}function o(a){return a.replace(/#/g,"%23").replace(/\n/g,"%0A")}function p(a){var b=r(a,"border-left-width"),c=r(a,"border-right-width");return a.scrollWidth+b+c}function q(a){var b=r(a,"border-top-width"),c=r(a,"border-bottom-width");return a.scrollHeight+b+c}function r(a,b){var c=window.getComputedStyle(a).getPropertyValue(b);return parseFloat(c.replace("px",""))}return{escape:l,parseExtension:b,mimeType:c,dataAsUrl:k,isDataUrl:d,canvasToBlob:f,resolveUrl:g,getAndEncode:j,uid:h(),delay:m,asArray:n,escapeXhtml:o,makeImage:i,width:p,height:q}}function n(){function a(a){return a.search(e)!==-1}function b(a){for(var b,c=[];null!==(b=e.exec(a));)c.push(b[1]);return c.filter(function(a){return!q.isDataUrl(a)})}function c(a,b,c,d){function e(a){return new RegExp("(url\\(['\"]?)("+q.escape(a)+")(['\"]?\\))","g")}return Promise.resolve(b).then(function(a){return c?q.resolveUrl(a,c):a}).then(d||q.getAndEncode).then(function(a){return q.dataAsUrl(a,q.mimeType(b))}).then(function(c){return a.replace(e(b),"$1"+c+"$3")})}function d(d,e,f){function g(){return!a(d)}return g()?Promise.resolve(d):Promise.resolve(d).then(b).then(function(a){var b=Promise.resolve(d);return a.forEach(function(a){b=b.then(function(b){return c(b,a,e,f)})}),b})}var e=/url\(['"]?([^'"]+?)['"]?\)/g;return{inlineAll:d,shouldProcess:a,impl:{readUrls:b,inline:c}}}function o(){function a(){return b(document).then(function(a){return Promise.all(a.map(function(a){return a.resolve()}))}).then(function(a){return a.join("\n")})}function b(){function a(a){return a.filter(function(a){return a.type===CSSRule.FONT_FACE_RULE}).filter(function(a){return r.shouldProcess(a.style.getPropertyValue("src"))})}function b(a){var b=[];return a.forEach(function(a){try{q.asArray(a.cssRules||[]).forEach(b.push.bind(b))}catch(c){console.log("Error while reading CSS rules from "+a.href,c.toString())}}),b}function c(a){return{resolve:function(){var b=(a.parentStyleSheet||{}).href;return r.inlineAll(a.cssText,b)},src:function(){return a.style.getPropertyValue("src")}}}return Promise.resolve(q.asArray(document.styleSheets)).then(b).then(a).then(function(a){return a.map(c)})}return{resolveAll:a,impl:{readAll:b}}}function p(){function a(a){function b(b){return q.isDataUrl(a.src)?Promise.resolve():Promise.resolve(a.src).then(b||q.getAndEncode).then(function(b){return q.dataAsUrl(b,q.mimeType(a.src))}).then(function(b){return new Promise(function(c,d){a.onload=c,a.onerror=d,a.src=b})})}return{inline:b}}function b(c){function d(a){var b=a.style.getPropertyValue("background");return b?r.inlineAll(b).then(function(b){a.style.setProperty("background",b,a.style.getPropertyPriority("background"))}).then(function(){return a}):Promise.resolve(a)}return c instanceof Element?d(c).then(function(){return c instanceof HTMLImageElement?a(c).inline():Promise.all(q.asArray(c.childNodes).map(function(a){return b(a)}))}):Promise.resolve(c)}return{inlineAll:b,impl:{newImage:a}}}var q=m(),r=n(),s=o(),t=p(),u={imagePlaceholder:void 0,cacheBust:!1},v={toSvg:b,toPng:d,toJpeg:e,toBlob:f,toPixelData:c,impl:{fontFaces:s,images:t,util:q,inliner:r,options:{}}};"undefined"!=typeof module?module.exports=v:a.domtoimage=v}(this);
// Scripts
const fabrique = document.querySelector('.button--shuffle');
const afficheTexte = document.querySelector('.contenu__texte');
const afficheGraphique = document.querySelector('.contenu__graphique');
const infoBtn = document.querySelector('.button--info');
const paysageBtn = document.querySelector('.button--paysage');
const info = document.querySelector('.informations');
const background = document.querySelector('.background');

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

function Affiche() {
  shuffleArray(art);
  let texteFinal = "";

  for (let i = 0; i < art[0].length; i++) {
    texteFinal += '<p>';
    texteFinal += art[0][i];
    texteFinal += '</p>';
  }

  afficheTexte.innerHTML = texteFinal;

  paint();

}

fabrique.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.remove('informations--show');
  Affiche();
});

infoBtn.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.toggle('informations--show');
});

function randomNb(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function paint() {

  const plusOrMinus = Math.random() < 0.5 ? -1 : 1;

  const existingEl = document.querySelectorAll('.contenu__graphique *, .background *');
  existingEl.forEach(function(el){
    el.remove();
  });

  const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svg.setAttributeNS(null, 'viewBox', '0 0 100 100');

  // Background
  const trapezoid = document.createElementNS('http://www.w3.org/2000/svg', 'path');

  const debutX = randomNb(20,80);
  const suiteX = randomNb(20,80);
  const finX = randomNb(0,100);
  const finY = randomNb(0,100);

  trapezoid.setAttributeNS(null, 'd', "M"+debutX+",0 L"+suiteX+",100 H200 V-100 L"+debutX+",0z");
  trapezoid.classList.add('trapeze');
  trapezoid.setAttributeNS(null, 'stroke', 'white');
  trapezoid.setAttributeNS(null, 'fill', 'white');
  if (plusOrMinus === 1) {
    trapezoid.setAttributeNS(null, 'transform', 'scale(-1, 1) translate(-100, 0)');
  }
  svg.appendChild(trapezoid);

  //small triangles
  for (var i = 0; i < randomNb(2,5); i++) {
    const plusOrMinusBis = Math.random() < 0.5 ? -1 : 1;
    const triangle = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    triangle.classList.add('triangle');
    triangle.setAttributeNS(null, 'points', ''+ randomNb(0,100)+','+randomNb(0,100)+' '+randomNb(0,100)+','+randomNb(0,100)+' '+randomNb(0,100)+','+randomNb(0,100));
    if (plusOrMinusBis === 1) {
      triangle.setAttributeNS(null, 'fill', 'black');
    } else {
      triangle.setAttributeNS(null, 'fill', 'white');
    }

    svg.appendChild(triangle);
  }

  const svgBg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svgBg.setAttributeNS(null, 'viewBox', '0 0 100 100');

  //triangles background
  for (var i = 0; i < randomNb(10,20); i++) {
    const triangleBg = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    triangleBg.classList.add('triangle');
    triangleBg.setAttributeNS(null, 'points', ''+ randomNb(-10,110)+','+randomNb(-10,110)+' '+randomNb(-10,110)+','+randomNb(-10,110)+' '+randomNb(-10,110)+','+randomNb(-10,110));
      triangleBg.setAttributeNS(null, 'fill', 'red');
    svgBg.appendChild(triangleBg);
  }

  background.appendChild(svgBg);

  afficheGraphique.appendChild(svg);

}

let downloadImage = document.querySelector('.button--img');
downloadImage.addEventListener('click', (e) => {
  e.preventDefault();
  document.documentElement.classList.add("hide-scrollbar");

var node = document.querySelector('.canvas');
const scale = 4200 / node.offsetWidth;

  domtoimage.toPng(node, {
        height: node.offsetHeight * scale,
    width: node.offsetWidth * scale,
    style: {
    transform: "scale(" + scale + ")",
    transformOrigin: "top left",
    width: node.offsetWidth + "px",
    height: node.offsetHeight + "px"
    }
  })
    .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'lart-des-dilutions.png';
        link.href = dataUrl;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    })
    .catch(function (error) {
        console.error('oops, something went wrong!', error);
    });

  document.documentElement.classList.remove("hide-scrollbar");
});

Affiche();
