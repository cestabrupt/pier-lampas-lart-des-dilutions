// Scripts
const fabrique = document.querySelector('.button--shuffle');
const afficheTexte = document.querySelector('.contenu__texte');
const afficheGraphique = document.querySelector('.contenu__graphique');
const infoBtn = document.querySelector('.button--info');
const paysageBtn = document.querySelector('.button--paysage');
const info = document.querySelector('.informations');
const background = document.querySelector('.background');

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

function Affiche() {
  shuffleArray(art);
  let texteFinal = "";

  for (let i = 0; i < art[0].length; i++) {
    texteFinal += '<p>';
    texteFinal += art[0][i];
    texteFinal += '</p>';
  }

  afficheTexte.innerHTML = texteFinal;

  paint();

}

fabrique.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.remove('informations--show');
  Affiche();
});

infoBtn.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.toggle('informations--show');
});

function randomNb(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function paint() {

  const plusOrMinus = Math.random() < 0.5 ? -1 : 1;

  const existingEl = document.querySelectorAll('.contenu__graphique *, .background *');
  existingEl.forEach(function(el){
    el.remove();
  });

  const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svg.setAttributeNS(null, 'viewBox', '0 0 100 100');

  // Background
  const trapezoid = document.createElementNS('http://www.w3.org/2000/svg', 'path');

  const debutX = randomNb(20,80);
  const suiteX = randomNb(20,80);
  const finX = randomNb(0,100);
  const finY = randomNb(0,100);

  trapezoid.setAttributeNS(null, 'd', "M"+debutX+",0 L"+suiteX+",100 H200 V-100 L"+debutX+",0z");
  trapezoid.classList.add('trapeze');
  trapezoid.setAttributeNS(null, 'stroke', 'white');
  trapezoid.setAttributeNS(null, 'fill', 'white');
  if (plusOrMinus === 1) {
    trapezoid.setAttributeNS(null, 'transform', 'scale(-1, 1) translate(-100, 0)');
  }
  svg.appendChild(trapezoid);

  //small triangles
  for (var i = 0; i < randomNb(2,5); i++) {
    const plusOrMinusBis = Math.random() < 0.5 ? -1 : 1;
    const triangle = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    triangle.classList.add('triangle');
    triangle.setAttributeNS(null, 'points', ''+ randomNb(0,100)+','+randomNb(0,100)+' '+randomNb(0,100)+','+randomNb(0,100)+' '+randomNb(0,100)+','+randomNb(0,100));
    if (plusOrMinusBis === 1) {
      triangle.setAttributeNS(null, 'fill', 'black');
    } else {
      triangle.setAttributeNS(null, 'fill', 'white');
    }

    svg.appendChild(triangle);
  }

  const svgBg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svgBg.setAttributeNS(null, 'viewBox', '0 0 100 100');

  //triangles background
  for (var i = 0; i < randomNb(10,20); i++) {
    const triangleBg = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    triangleBg.classList.add('triangle');
    triangleBg.setAttributeNS(null, 'points', ''+ randomNb(-10,110)+','+randomNb(-10,110)+' '+randomNb(-10,110)+','+randomNb(-10,110)+' '+randomNb(-10,110)+','+randomNb(-10,110));
      triangleBg.setAttributeNS(null, 'fill', 'red');
    svgBg.appendChild(triangleBg);
  }

  background.appendChild(svgBg);

  afficheGraphique.appendChild(svg);

}

let downloadImage = document.querySelector('.button--img');
downloadImage.addEventListener('click', (e) => {
  e.preventDefault();
  document.documentElement.classList.add("hide-scrollbar");

var node = document.querySelector('.canvas');
const scale = 4200 / node.offsetWidth;

  domtoimage.toPng(node, {
        height: node.offsetHeight * scale,
    width: node.offsetWidth * scale,
    style: {
    transform: "scale(" + scale + ")",
    transformOrigin: "top left",
    width: node.offsetWidth + "px",
    height: node.offsetHeight + "px"
    }
  })
    .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'lart-des-dilutions.png';
        link.href = dataUrl;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    })
    .catch(function (error) {
        console.error('oops, something went wrong!', error);
    });

  document.documentElement.classList.remove("hide-scrollbar");
});

Affiche();
