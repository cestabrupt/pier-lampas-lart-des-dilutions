﻿const art = [
  [
    "ayons l’art enfin des&nbsp;stratégies&nbsp;éruptives",
  ],
  [
    "mon chant est immanent&nbsp;:",
    "il&nbsp;perce sous&nbsp;l’épiderme",
  ],
  [
    "bruto&nbsp;!",
  ],
  [
    "ignores-tu que&nbsp;le&nbsp;poème a&nbsp;pour&nbsp;vocation de&nbsp;se&nbsp;fondre au&nbsp;cœur des&nbsp;choses afin&nbsp;de&nbsp;les&nbsp;éclairer&nbsp;?",
    "et que l’action&nbsp;révolutionnaire établit les&nbsp;conditions&nbsp;matérielles de&nbsp;cette&nbsp;lumière&nbsp;?",
  ],

  [
    "c’est ici bas que&nbsp;nous&nbsp;nous&nbsp;battrons",
  ],
  [
    "les&nbsp;passions nerveuses ne&nbsp;nous&nbsp;durent&nbsp;pas",
    "aucune&nbsp;étoile au-dessus de&nbsp;nos&nbsp;têtes",
    "mille soleils, et&nbsp;c’est&nbsp;tout",
  ],
  [
    "c’est la&nbsp;caresse enflammée d’un&nbsp;soleil vengeur sur&nbsp;le&nbsp;front de&nbsp;nos&nbsp;lâchetés",
  ],
  [
    "car en&nbsp;l’absence de&nbsp;géniteurs&nbsp;crédibles et&nbsp;de&nbsp;racines, nous&nbsp;sommes&nbsp;libres",
  ],
  [
    "l’alchimie singulière des&nbsp;enfants du&nbsp;printemps ne&nbsp;suppose aucun&nbsp;relâchement",
  ],
  [
    "il ne&nbsp;s’agira&nbsp;pas de&nbsp;grossir les&nbsp;rangs de&nbsp;la&nbsp;pestilente&nbsp;postmodernité&nbsp;:",
    "notre&nbsp;dilution n’implique&nbsp;aucunement l’abolition des&nbsp;hommes et&nbsp;de&nbsp;leur&nbsp;histoire",
    "mais cette&nbsp;histoire sera déterritorialisée, profane, décoloniale",
  ],
  [
    "car nous sommes tous sœurs&nbsp;et&nbsp;frères de&nbsp;floraison&nbsp;!",
    "et à&nbsp;ce&nbsp;titre&nbsp;: entièrement vierge de&nbsp;rancœur",
  ],
  [
    "et nous&nbsp;enfanterons des&nbsp;trésors sans&nbsp;aucune&nbsp;larme&nbsp;amère,",
    "et des&nbsp;champs de&nbsp;violettes et&nbsp;des&nbsp;soleils&nbsp;de&nbsp;dune",
  ],
  [
    "nous&nbsp;nous&nbsp;ferons artisans des&nbsp;entre-chocs et&nbsp;des&nbsp;entre-mondes&nbsp;!",
  ],
  [
    "notre&nbsp;théâtre&nbsp;est celui&nbsp;des&nbsp;passions&nbsp;animales",
    "et c’est&nbsp;le&nbsp;comble du&nbsp;raffinement",
  ],
  [
    "dans chaque détour, je&nbsp;rencontre&nbsp;la&nbsp;nécessité",
    "d’en finir&nbsp;avec cette&nbsp;société&nbsp;de&nbsp;classes",
  ],
  [
    "et si l’âpreté du&nbsp;monde justifie notre méfiance&nbsp;de&nbsp;l’aube,",
    "prenons pour&nbsp;modèle l’éclosion&nbsp;des&nbsp;bourgeons",
  ],
  [
    "car ici, il reste des&nbsp;hommes, il reste des&nbsp;femmes, et&nbsp;des&nbsp;drapeaux&nbsp;troués",
    "et des&nbsp;soleils&nbsp;vengeurs, et&nbsp;des&nbsp;éclats de&nbsp;poudre et&nbsp;des&nbsp;sentiers&nbsp;rageurs",
  ],
  [
    "cette renaissance nous&nbsp;ouvre la&nbsp;voie d’un&nbsp;monde déjà&nbsp;existant,",
    "un&nbsp;monde opprimé&nbsp;et&nbsp;suffocant,",
    "mais dont&nbsp;la&nbsp;lumière plurielle&nbsp;et&nbsp;viscérale ne&nbsp;saurait taire son&nbsp;évidence",
  ],
  [
    "je&nbsp;suis un&nbsp;champ de&nbsp;lavande",
    "qu’irradie le&nbsp;vin des&nbsp;révolutions",
  ],
  [
    "dans la&nbsp;révolte&nbsp;jaune",
    "et les&nbsp;refrains&nbsp;fatigués",
    "des&nbsp;vieux cortèges&nbsp;syndicaux",
  ],
  [
    "dans ces&nbsp;hymnes&nbsp;vieillots",
    "et dans&nbsp;la&nbsp;résurgence",
    "de la&nbsp;conscience de&nbsp;classe",
  ],
  [
    "dans l’exacerbation du&nbsp;mépris&nbsp;d’état",
    "et dans le&nbsp;dernier&nbsp;sursaut",
    "de la&nbsp;dignité&nbsp;éborgnée",
  ],
  [
    "défoncé à&nbsp;grand&nbsp;coup",
    "de chariot élévateur",
  ],
  [
    "c’est sous&nbsp;le&nbsp;patronage de&nbsp;l’errance&nbsp;et&nbsp;de&nbsp;la&nbsp;déshérence",
    "que je&nbsp;compose mon&nbsp;chant,",
    "c’est la&nbsp;chair impersonnelle et&nbsp;irresponsable qui&nbsp;me&nbsp;convoque",
  ],
  [
    "et j’envie&nbsp;l’innommé",
    "autant que&nbsp;l’innommable",
  ],
  [
    "le monde est un&nbsp;ogre&nbsp;affamé,",
    "je&nbsp;veux qu’il&nbsp;m’avale sans&nbsp;ménagement",
  ],
  [
    "je&nbsp;veux que ma&nbsp;dilution soit&nbsp;indigeste,",
    "qu’il me recrache en&nbsp;mille&nbsp;lambeaux",
  ],
  [
    "je&nbsp;ne serai qu’échappées et&nbsp;gueules&nbsp;ouvertes,",
    "et je me diluerai dans&nbsp;les&nbsp;régions&nbsp;de&nbsp;l’infâme",
    "en me moquant de&nbsp;nos&nbsp;chairs&nbsp;suppliciées",
  ],
  [
    "car l’espérance, j’abdique",
    "je n’ai plus le&nbsp;temps",
    "et je préfère le&nbsp;vin",
  ],
  [
    "suis-je trop sévère avec&nbsp;les&nbsp;étoiles&nbsp;?",
    "je ne le&nbsp;crois&nbsp;pas",
    "c’est que&nbsp;l’âme d’un&nbsp;voyou me&nbsp;paraît&nbsp;plus&nbsp;honnête",
  ],
  [
    "ville crasse et&nbsp;théâtre des&nbsp;atrocités&nbsp;ordinaires",
  ],
  [
    "dans l’histoire&nbsp;concrète d’une&nbsp;main&nbsp;qui&nbsp;se&nbsp;déploie",
    "sur un&nbsp;fusil&nbsp;mitrailleur,",
    "sur ses&nbsp;munitions de&nbsp;perles et&nbsp;de&nbsp;jacinthes...",
  ],
  [
    "construisons méthodiquement",
    "les conditions d’une&nbsp;dialectique",
    "de&nbsp;la&nbsp;terre et&nbsp;du&nbsp;ciel",
  ],
  [
    "les étoiles ne&nbsp;pourront&nbsp;plus se&nbsp;moquer",
    "car elles baigneront dans&nbsp;la&nbsp;même&nbsp;fange",
    "et dans&nbsp;la&nbsp;même&nbsp;ivresse que&nbsp;nous",
  ],
  [
    "mais il&nbsp;génère&nbsp;aussi la&nbsp;grâce",
    "n’ayons plus jamais l’art des&nbsp;compromissions&nbsp;!",
  ],
  [
    "et nous combattrons&nbsp;ainsi le&nbsp;discours&nbsp;managérial",
    "appliqué à&nbsp;la floraison des&nbsp;êtres&nbsp;et&nbsp;des&nbsp;choses",
  ],
  [
    "je veux vivre en&nbsp;bonne&nbsp;intelligence",
    "avec le&nbsp;ficus&nbsp;sauvage faisant&nbsp;corps",
    "avec le&nbsp;pavé&nbsp;napolitain",
  ],
  [
    "c’est que ce&nbsp;poème&nbsp;sera radicalement un&nbsp;poème",
    "de douceur&nbsp;napolitaine et&nbsp;de&nbsp;violence nécessaire",
    "d’amour et&nbsp;de&nbsp;douceur révolutionnaires&nbsp;!",
  ],
  [
    "et ma&nbsp;soif aiguise son&nbsp;objet&nbsp;:",
    "nous&nbsp;sommes le&nbsp;monde",
  ],
  [
    "car l’alchimie&nbsp;moléculaire éventre les&nbsp;destins&nbsp;biaisés",
  ],
  [
    "la chair vive et&nbsp;passionnée",
    "d’un&nbsp;amour qui&nbsp;se&nbsp;confond avec&nbsp;la&nbsp;rage",
  ],
  [
    "dans chaque&nbsp;détour, l’histoire&nbsp;!",
  ],
  [
    "car il&nbsp;faut fondre nietzsche&nbsp;dans&nbsp;marx&nbsp;!",
    "et marx dans&nbsp;rimbaud&nbsp;!",
  ],
  [
    "il&nbsp;faut jeter platon dans&nbsp;les orties&nbsp;!",
    "et installer la&nbsp;poésie dans&nbsp;le&nbsp;cœur&nbsp;des&nbsp;choses&nbsp;!",
  ],
  [
    "nous&nbsp;sommes marxistes et&nbsp;amoureux des&nbsp;précipices&nbsp;!",
  ],
  [
    "c’est une&nbsp;histoire d’herbes&nbsp;folles",
    "dans le&nbsp;cimetière&nbsp;profane du&nbsp;testaccio",
  ]
];
